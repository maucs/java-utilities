(function (window, document) {

    var layout = document.getElementById('layout'),
        menuLink = document.getElementById('menuLink'),
        main = document.getElementById('main');

    // can use element.classList
    function toggleClass(element, className) {
        var classes = element.className.split(/\s+/),
            length = classes.length,
            i = 0;

        for (; i < length; i++) {
            if (classes[i] === className) {
                classes.splice(i, 1);
                break;
            }
        }
        // The className is not found
        if (length === classes.length) {
            classes.push(className);
        }

        element.className = classes.join(' ');
    }

    function toggleAll(e) {
        var active = 'active';

        e.preventDefault();
        toggleClass(layout, active);
        // the purecss example toggles more ids, you may want that if
        // your css file targets these elements
    }

    menuLink.onclick = function (e) {
        toggleAll(e);
    };

    // cannot use #layout because it's on top of #menuLink, and thus takes precedence over it
    main.onclick = function (e) {
        // if layout doesn't have the class 'active'
        if (layout.className.indexOf('active') !== -1) {
            toggleAll(e);
        }
    };

}(this, this.document));