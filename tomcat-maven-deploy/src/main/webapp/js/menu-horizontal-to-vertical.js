(function (window, document) {
    var menu = document.getElementById('menu'),
        toggle = document.getElementById('toggle'),
        WINDOW_CHANGE_EVENT = ('onorientationchange' in window) ? 'orientationchange' : 'resize';

    function toggleHorizontal() {
        [].forEach.call(menu.querySelectorAll('.custom-can-transform'), function (el) {
                el.classList.toggle('pure-menu-horizontal');
            }
        );
    }

    function toggleMenu() {
        // set timeout so that the panel has a chance to roll up
        // before the menu switches states
        // tldr: you don't want the words to instantly line-up horizontally when menu is closing
        if (menu.classList.contains('open')) {
            setTimeout(toggleHorizontal, 500);
        }
        else {
            toggleHorizontal();
        }
        menu.classList.toggle('open');
    }

    toggle.addEventListener('click', function (e) {
        toggleMenu();
        e.preventDefault();
    });

    // close menu if window resize
    window.addEventListener(WINDOW_CHANGE_EVENT, function () {
        if (menu.classList.contains('open')) {
            toggleMenu();
        }
    });
})(this, this.document);

