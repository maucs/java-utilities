# Executable with Maven Shade

Run `mvn package`, the resulting jar is in the folder **target**, and it can be executed with `java -jar executable-shade.jar 1 2 3`