package ex;

import java.sql.Connection;
import java.sql.DriverManager;

public class Main {
    public static void main(String[] args) throws Exception {
        for (String arg : args)
            System.out.println(arg);
        Class.forName("org.h2.Driver");
        try (Connection conn = DriverManager.getConnection("jdbc:h2:mem:")) {
            System.out.println(conn.toString());
        }
    }
}