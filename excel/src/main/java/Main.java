import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) throws IOException {
        read();
        write();
    }

    static void read() throws IOException {
        // reads xlsx files, not xls
        try (XSSFWorkbook sheets = new XSSFWorkbook("read.xlsx")) {
            XSSFSheet sheet = sheets.getSheetAt(0);
            Iterator<Row> rows = sheet.rowIterator();
            rows.next(); // ignore first row
            // can use cells.getCell(0).getCellTypeEnum()
            rows.forEachRemaining(cells -> System.out.printf("%s, age %s, work as a %s\n",
                    cells.getCell(0).getStringCellValue(),
                    cells.getCell(1).getNumericCellValue(),
                    cells.getCell(2).getStringCellValue()));
        }
    }

    static void write() throws IOException {
        try (FileOutputStream out = new FileOutputStream("output.xlsx");
             XSSFWorkbook workbook = new XSSFWorkbook()) {
            // init the rows, columns. used to fill values
            Row row;
            Cell cell;

            XSSFSheet s = workbook.createSheet("new sheet name");

            // set font for header
            CellStyle style = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);

            // header
            row = s.createRow(0);
            for (int i = 0; i < 5; i++) {
                cell = row.createCell(i);
                cell.setCellStyle(style);
                cell.setCellValue("Power " + (i + 1));
            }

            // fill the values
            for (int i = 1; i < 21; i++) {
                row = s.createRow(i);
                for (int j = 0; j < 5; j++) {
                    cell = row.createCell(j);
                    switch (j) {
                        case 0:
                            cell.setCellValue(i);
                            break;
                        case 1:
                            cell.setCellValue(Math.pow(i, 2));
                            break;
                        case 2:
                            cell.setCellValue(Math.pow(i, 3));
                            break;
                        case 3:
                            cell.setCellValue(Math.pow(i, 4));
                            break;
                        case 4:
                            cell.setCellValue(Math.pow(i, 5));
                            break;
                    }
                }
            }
            workbook.write(out);
        }
    }
}
