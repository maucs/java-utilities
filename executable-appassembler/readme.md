# Executable with Maven Appassembler

Run `mvn package appassembler:assemble` (must run both at the same time), and then (in the **target/assembler** folder) run `./bin/my-great-app 1 2 3`