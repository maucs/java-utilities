package entity;

public class Dog {
    public String name;
    public int age;
    public boolean bitable;

    public Dog(String name, int age, boolean bitable) {
        this.name = name;
        this.age = age;
        this.bitable = bitable;
    }
}
