import javax.json.*;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;

public class MainJsonP {
    public static void main(String[] args) {
        streamWrite();
        streamRead();
    }

    private static void objectWrite() {
        String s1 = Json.createObjectBuilder()
                .add("name", "Job")
                .add("age", BigDecimal.ONE).build().toString();
        System.out.println(s1);

        String s2 = Json.createArrayBuilder().add(10).add(20).add("bob").addNull().add(false).build().toString();
        System.out.println(s2);

        // write, optional but useful
        StringWriter writer = new StringWriter();
        JsonWriter jsonWriter = Json.createWriter(writer);
        jsonWriter.writeObject(Json.createObjectBuilder().build());
        jsonWriter.close();
    }

    private static void objectRead() {
        JsonReader reader = Json.createReader(getResourceAsStream());
        JsonStructure structure = reader.read();
        JsonValue value = structure.getValue("/name");
        System.out.println(value.getValueType() + " - " + value.toString());
        JsonValue bool = structure.getValue("/rich");
        System.out.println(bool.getValueType() + " - " + bool.toString());
    }

    private static void streamWrite() {
        StringWriter writer = new StringWriter();
        JsonGenerator gen = Json.createGenerator(writer);
        gen.writeStartObject()
                .write("name", "John")
                .writeStartArray("list").write(10).write(20).writeEnd()
                .writeEnd();
        gen.close();
        System.out.println(writer.toString());
    }

    private static void streamRead() {
        JsonParser parser = Json.createParser(getResourceAsStream());
        while (parser.hasNext()) {
            JsonParser.Event event = parser.next();
            switch (event) {
                case START_ARRAY:
                case END_ARRAY:
                case START_OBJECT:
                case END_OBJECT:
                case VALUE_NUMBER:
                case VALUE_TRUE:
                case VALUE_FALSE:
                case VALUE_NULL:
                    System.out.println("Other(Event Type): " + event.name());
                    break;
                case KEY_NAME:
                    System.out.println("Key: " + parser.getString());
                    break;
                case VALUE_STRING:
                    System.out.println("Value(String): " + parser.getString());
                    break;

            }
        }
    }

    private static InputStream getResourceAsStream() {
        return MainJsonP.class.getResourceAsStream("file.json");
    }
}
