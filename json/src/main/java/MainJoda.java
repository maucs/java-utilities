import entity.DogBean;
import org.joda.beans.Bean;
import org.joda.beans.ser.JodaBeanSer;

public class MainJoda {
    public static void main(String[] args) {
        Bean dog = new DogBean("Jfar", 1, true);
        System.out.println(JodaBeanSer.PRETTY.simpleJsonWriter().write(dog));
        System.out.println(JodaBeanSer.PRETTY.xmlWriter().write(dog));
    }
}
