import entity.Dog;

import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import java.util.ArrayList;
import java.util.Arrays;

public class MainJsonB {
    public static void main(String[] args) {
        EntityToJson();
    }

    private static void EntityToJson() {
        Dog dog = new Dog("Jaer", 5, true);
        String entity = JsonbBuilder.create().toJson(dog);
        System.out.println(entity);
        JsonbBuilder.create().fromJson(entity, Dog.class);

        String list = JsonbBuilder.create().toJson(Arrays.asList(
                new Dog("Jin", 2, false),
                new Dog("Nif", 1, false),
                new Dog("Marleh", 10, true)));
        System.out.println(list);
        JsonbBuilder.create().fromJson(list, new ArrayList<Dog>() {
        }.getClass().getGenericSuperclass());

        // custom config, if not set at entity level
        JsonbConfig config = new JsonbConfig().withNullValues(true);
        JsonbBuilder.create(config);
    }
}
