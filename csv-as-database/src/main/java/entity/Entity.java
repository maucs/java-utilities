package entity;

import javax.persistence.Id;

@javax.persistence.Entity
public class Entity {
    private String name, address, size, people;

    public Entity() {
    }

    @Override
    public String toString() {
        return "Entity{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", size='" + size + '\'' +
                ", people='" + people + '\'' +
                '}';
    }

    @Id
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }
}
