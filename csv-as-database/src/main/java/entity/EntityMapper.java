package entity;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EntityMapper {
    @Insert("CREATE TABLE data AS SELECT * FROM CSVREAD('read.csv')")
    void populateTable();

    @Select("SELECT * FROM data")
    List<Entity> getAllEntities();

    @Insert("CALL CSVWRITE('output-sql.csv', 'SELECT * FROM data')")
    void exportToCsv();
}
