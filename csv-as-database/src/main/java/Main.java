import entity.Entity;
import entity.EntityMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.apache.ibatis.io.Resources.getResourceAsStream;

public class Main {
    public static void main(String[] args) throws Exception {
        jpaFill();
    }

    private static void jpaCreate() throws Exception {
        EntityManager em = Persistence.createEntityManagerFactory("unit-create").createEntityManager();
        EntityTransaction tx = em.getTransaction();

        tx.begin();
        em.createNativeQuery("CREATE TABLE Entity AS SELECT * FROM CSVREAD('read.csv')").executeUpdate();
        em.createQuery("SELECT e FROM Entity e", Entity.class)
                .getResultList()
                .forEach(System.out::println);
        em.createNativeQuery("CALL CSVWRITE('output-sql.csv', 'SELECT * FROM Entity')").executeUpdate();
        tx.commit();
    }

    private static void jpaFill() throws Exception {
        EntityManager em = Persistence.createEntityManagerFactory("unit-fill").createEntityManager();
        EntityTransaction tx = em.getTransaction();

        tx.begin();
        em.createNativeQuery("INSERT INTO Entity(name, address, size, people) SELECT * FROM CSVREAD('read.csv')").executeUpdate();
        em.createQuery("SELECT e FROM Entity e", Entity.class)
                .getResultList()
                .forEach(System.out::println);
        tx.commit();
    }

    private static void mybatis() throws IOException {
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(getResourceAsStream("mybatis.xml"));
        try (SqlSession session = sqlSessionFactory.openSession();) {
            EntityMapper mapper = session.getMapper(EntityMapper.class);
            mapper.populateTable();
            mapper.getAllEntities().forEach(System.out::println);
            mapper.exportToCsv();
        }
    }

    private static void vanillaJDBC() throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        try (Connection conn = DriverManager.getConnection("jdbc:h2:mem:")) {
            conn.createStatement().execute("CREATE TABLE data AS SELECT * FROM CSVREAD('read.csv')");
            ResultSet resultSet = conn.createStatement().executeQuery("SELECT * FROM data");
            while (resultSet.next())
                System.out.printf("%s - %s - %s - %s\n",
                        resultSet.getString(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4));
            conn.createStatement().execute("CALL CSVWRITE('output-sql.csv', 'SELECT * FROM data')");
        }
    }
}
