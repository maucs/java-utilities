# CSV as a Database

## JPA

An advantage of adding the **javax.persistence** dependency is that changing libraries is easy

When using H2 to insert data, make sure the name of the table that the data is inserted is correct

Some workaround

1. Set table name in the **@Entity** field
2. Set **eclipselink.ddl-generation** to **none**, and create the table
3. Set **eclipselink.ddl-generation** to **drop-and-create-tables** and fill the table