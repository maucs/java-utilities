import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.print.*;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.util.Duration;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;

import java.awt.image.BufferedImage;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

// Awt filechoose doesn't work
public class FXMLExampleController implements javafx.fxml.Initializable {

    public Button updateButton;
    public Label status;
    public Button printButton;
    public Button printDirectButton;
    public TextArea log;
    private List<Button> buttons;

    // implicit if you didn't implement the Interface
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        buttons = Arrays.asList(updateButton, printDirectButton);

        methodOneA();
    }

    public void openFileChooser(ActionEvent e) {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open File");
        File file = chooser.showOpenDialog(getWindow(e));

        if (file == null) {
            log.appendText("Cancelling \"Open File\"\n");
        } else {
            log.appendText(file.getAbsolutePath() + "\n");
        }
    }

    public void printSomething(ActionEvent e) throws IOException {
        // all these just to convert it to image
        // https://pdfbox.apache.org/2.0/migration.html
        PDDocument pdf = PDDocument.load(new File("read.pdf"));
        PDFRenderer pdfRenderer = new PDFRenderer(pdf);
        int pageCounter = 0;
        for (PDPage page : pdf.getPages()) {
            BufferedImage bim = pdfRenderer.renderImageWithDPI(pageCounter, 300, ImageType.RGB);
            // doesn't really write to resources folder, this is only an example
            ImageIOUtil.writeImage(bim, "output.png", 300);
        }

        // javafx printerjob only deals with javafx nodes
        PrinterJob printerJob = PrinterJob.createPrinterJob();
        // page layout is optional
        PageLayout pageLayout = printerJob.getPrinter().createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.DEFAULT);
        if (printerJob.showPrintDialog(getWindow(e))) {
            ImageView imageView = new ImageView("output.png");
            /// without this, the image doesn't use up the entire area
            imageView.setPreserveRatio(true);
            imageView.setFitHeight(pageLayout.getPrintableHeight());
            imageView.setFitWidth(pageLayout.getPrintableWidth());
            ///
            if (printerJob.printPage(pageLayout, imageView)) {
                System.out.println("DONE");
                printerJob.endJob();
            }
        }
    }

    public void printAwt(ActionEvent e) {
        // if not done in a new thread, javafx will freeze
        // https://pdfbox.apache.org/2.0/migration.html
        new Thread(() -> {
            java.awt.print.PrinterJob job = java.awt.print.PrinterJob.getPrinterJob();
            try (PDDocument doc = PDDocument.load(new File("read.pdf"))) {
                job.setJobName("A nice job name you'll see in your printer queue");
                job.setPageable(new PDFPageable(doc));
                if (job.printDialog()) {
                    job.print();
                } else {
                    log.appendText("Cancel printing");
                }
            } catch (IOException | PrinterException ignored) {
            }
        }).start();
    }

    private Window getWindow(ActionEvent event) {
        return ((Node) event.getSource()).getScene().getWindow();
    }

    ///////////
    ///////////
    // This is the start of all things concurrency
    ///////////
    ///////////

    /**
     * This is the basic framework, using task.setOnSucceeded
     * <p>
     * The order of task executed is flexible, rearrange it anyway you like
     */
    public void methodOne() {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Thread.sleep(2000);
                return null;
            }
        };
        task.setOnSucceeded(e -> {
            status.setText("Updated! Have Fun!");
            buttons.forEach(button -> button.setDisable(false));
        });
        updateButton.setOnAction(e -> {
            buttons.forEach(button -> button.setDisable(true));
            status.setText("Updating text, please wait...");
            new Thread(task).start();
        });
    }

    /**
     * Using {@link Service} to create generate a Task, and
     * {@link Bindings} to change the state of buttons
     * <p>
     * Calling {@link Service#start()} runs in a new Thread automatically.
     * Call {@link Platform#runLater(Runnable)} to update GUI within the task
     */
    public void methodOneA() {
        Service<Void> service = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        Thread.sleep(2000);
                        return null;
                    }
                };
            }
        };
        updateButton.disableProperty().bind(
                Bindings.when(service.runningProperty())
                        .then(true)
                        .otherwise(false));
        service.setOnSucceeded(e -> {
            status.setText("Updated! Have Fun!");
        });
        updateButton.setOnAction(e -> {
            status.setText("Updating text, please wait...");
            service.restart(); // don't use start() if you want to reuse the service https://stackoverflow.com/a/36893429
        });
    }

    /**
     * {@link Platform#runLater(Runnable)} will run the thread in the main
     * JavaFX thread, eventually
     * <p>
     * The simplest way of doing things, it can be used together with any of these methods
     */
    public void methodTwo() {
        updateButton.setOnAction(e -> new Thread(() -> {
            try {
                // runLater runs this in the same JavaFX GUI thread
                Platform.runLater(() -> {
                    buttons.forEach(button -> button.setDisable(true));
                    status.setText("Updating text, please wait...");
                });
                Thread.sleep(2000);
                Platform.runLater(() -> {
                    status.setText("Updated! Have Fun!");
                    buttons.forEach(button -> button.setDisable(false));
                });
            } catch (InterruptedException e1) {
            }
        }).start());
    }

    /**
     * ??? The status label is in a different thread?
     */
    public void methodThree() {
        UpdateTask updateTask = new UpdateTask();
        updateButton.disableProperty().bind(updateTask.disableButtonProperty());
        status.textProperty().bind(updateTask.statusProperty());
        updateButton.setOnAction(e -> {
            new Thread(() -> {
                try {
                    updateTask.call();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }).start();
        });
    }

    /**
     * Using the Animation API, no direct usage of Threads API
     * <p>
     * https://stackoverflow.com/a/26477294 method 'animateUsingScaleTransition' is not used
     * <p>
     * Take advantage of the timeline API if you decide on using this. If you want to run a Thread,
     * run a new Thread like in {@link #methodTwo()}
     */
    public void methodFour() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.ZERO, e -> {
                    updateButton.setDisable(true);
                    status.setText("Updating text, please wait...");
                }),
                new KeyFrame(Duration.seconds(2), e -> {
                    updateButton.setDisable(false);
                    status.setText("Updated! Have Fun!");
                })
        );
        updateButton.setOnAction(e -> {
            timeline.playFromStart();
        });
    }

    private class UpdateTask extends Task<Void> {
        private ReadOnlyBooleanWrapper disableButton;
        private ReadOnlyStringWrapper status;

        public UpdateTask() {
            disableButton = new ReadOnlyBooleanWrapper();
            status = new ReadOnlyStringWrapper("Status");
        }

        public ReadOnlyBooleanProperty disableButtonProperty() {
            return disableButton.getReadOnlyProperty();
        }

        public ReadOnlyStringProperty statusProperty() {
            return status.getReadOnlyProperty();
        }

        @Override
        protected Void call() throws Exception {
            status.set("Updating...");
            disableButton.set(true);
            Thread.sleep(2000);
            disableButton.set(false);
            status.set("Updated!");
            return null;
        }
    }
}
