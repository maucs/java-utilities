import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

import javax.swing.*;
import java.awt.*;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Main extends JPanel {
    private JButton updateButton = new JButton("Update Text");
    private JButton openSwingButton = new JButton("Open Swing FileChooser");
    private Button openAwtButton = new Button("Open Awt FileChooser");
    private Button printButton = new Button("Print something");
    private Button printDirectButton = new Button("Print directly");
    private JLabel label = new JLabel("No event...");
    private JTextArea log = new JTextArea(5, 20);
    ;

    private Main() {
        super(new BorderLayout());
        //Create the log first, because the action listeners need to refer to it.
        log.setMargin(new Insets(5, 5, 5, 5));
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);

        // you can implement an ActionListener class, and use e.source == updateButton
        updateButton.addActionListener(e -> {
            updateButton.setEnabled(false);
            openSwingButton.setEnabled(false);
            openAwtButton.setEnabled(false);
            label.setText("Updating... Please wait");
            new Perform().execute();
        });

        openSwingButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            int result = fileChooser.showOpenDialog(Main.this);
            if (result == JFileChooser.APPROVE_OPTION) {
                String absolutePath = fileChooser.getSelectedFile().getAbsolutePath();
                appendLog(absolutePath);
            } else {
                appendLog("Cancelling \"Open File\"");
            }
        });

        openAwtButton.addActionListener(e -> {
            FileDialog fileDialog = new FileDialog((JFrame) null);
            fileDialog.setVisible(true);
            if (fileDialog.getFile() != null) {
                appendLog(fileDialog.getDirectory() + fileDialog.getFile());
            } else {
                appendLog("Cancelling \"Open File\"");
            }
        });

        printButton.addActionListener(e -> {
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setJobName("A nice job name you'll see in your printer queue");
            try (PDDocument doc = PDDocument.load(new File("read.pdf"))) {
                job.setPageable(new PDFPageable(doc));
                if (job.printDialog()) {
                    job.print();
                    appendLog("Printing...");
                } else {
                    appendLog("Cancelling print");
                }
            } catch (IOException | PrinterException e1) {
            }
        });

        printDirectButton.addActionListener(e -> {
            PrinterJob job = PrinterJob.getPrinterJob();
            try (PDDocument doc = PDDocument.load(new File("read.pdf"))) {
                job.setPageable(new PDFPageable(doc));
                job.print();
                appendLog("Printing...");
            } catch (IOException | PrinterException e1) {
            }
        });


        //For layout purposes, put the buttons in a separate panel
        JPanel buttonPanel = new JPanel(); //use FlowLayout
        buttonPanel.add(updateButton);
        buttonPanel.add(openSwingButton);
        buttonPanel.add(openAwtButton);
        buttonPanel.add(printButton);
        buttonPanel.add(printDirectButton);

        //Add the buttons and the log to this panel.
        add(buttonPanel, BorderLayout.PAGE_START);
        add(logScrollPane, BorderLayout.CENTER);
        add(label, BorderLayout.PAGE_END);
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("FileChooserDemo");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //Add content to the window.
        frame.add(new Main());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(Main::createAndShowGUI);
    }

    private void appendLog(String msg) {
        log.append(msg + "\n");
        log.setCaretPosition(log.getDocument().getLength());
    }

    private class Perform extends SwingWorker<String, Void> {
        @Override
        protected String doInBackground() throws Exception {
            Thread.sleep(2000);
            return "Event has update this";
        }

        @Override
        protected void done() {
            super.done();
            try {
                label.setText(get());
            } catch (InterruptedException | ExecutionException e1) {
                e1.printStackTrace();
            }
            updateButton.setEnabled(true);
            openSwingButton.setEnabled(true);
            openAwtButton.setEnabled(true);
        }
    }
}
