import java.io.BufferedReader;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.readFromResources();
    }

    private void readFromResources() throws Exception {
        // https://stackoverflow.com/a/326440
        URL resource = getClass().getResource("file.txt");

        // ez
        System.out.println("=== Method 1");
        byte[] s1 = Files.readAllBytes(Paths.get(resource.toURI()));
        System.out.println(new String(s1));

        // ez #2
        List<String> strings = Files.readAllLines(Paths.get(resource.toURI()));
        strings.forEach(s -> System.out.println(s.toUpperCase()));

        // bufferedreader, efficient
        BufferedReader reader = Files.newBufferedReader(Paths.get(resource.toURI()));
        reader.lines().forEach(s -> System.out.println(s.toUpperCase()));

        // hard way
        InputStream resourceFile = getClass().getResourceAsStream("file.txt");
        java.util.Scanner s = new java.util.Scanner(resourceFile).useDelimiter("\\A");
        String full = s.hasNext() ? s.next() : "";
        System.out.println(full);
    }
}
