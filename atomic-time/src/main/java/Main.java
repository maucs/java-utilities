import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicReference;

public class Main {
    private static AtomicReference<LocalDateTime> time = new AtomicReference<>(LocalDateTime.MIN);

    public static void main(String[] args) throws InterruptedException {
        while (true) {
            action();
            Thread.sleep(1500);
        }
    }

    private static void action() {
        LocalDateTime oldTime = time.getAndUpdate(t -> {
            LocalDateTime now = LocalDateTime.now();
            if (now.isAfter(t))
                return now.plusSeconds(5);
            return t;
        });

        LocalDateTime newTime = time.get();

        if (!oldTime.equals(newTime)) {
            // action
            System.out.println("The new time is " + newTime);
        } else
            System.out.println("Not the time yet");
    }
}
