import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import entity.Box;
import entity.House;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        List<Box> boxes = new ArrayList<>();
        boxes.add(new Box("Barry", "big", "black"));
        boxes.add(new Box("Trpl", "small", "green"));
        boxes.add(new Box("Z1", "medium", "marble"));
        boxes.add(new Box("N0", "big", "orange"));

        // write
        try (CSVWriter writer = new CSVWriter(new FileWriter("output1.csv"))) {
            String[] entries = "first#second#third".split("#");
            writer.writeNext(entries);
        }

        try (Writer writer = new FileWriter("output2.csv")) {
            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
            beanToCsv.write(boxes);
        }

        // read
        List<House> houses = new CsvToBeanBuilder(new FileReader("read.csv"))
                .withType(House.class)
                .build()
                .parse();
        houses.forEach(house -> System.out.printf("%s: %s (%s)\n", house.getName(), house.getAddress(), house.getSize()));
    }
}
