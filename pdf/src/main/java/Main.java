import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;

public class Main {
    private static final int FONT_SIZE = 15;

    public static void main(String[] args) throws IOException {
        generatePDF();
        readPDF();
    }

    private static void generatePDF() throws IOException {
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage(PDRectangle.A6);

            try (PDPageContentStream stream = new PDPageContentStream(doc, page)) {
                stream.beginText();
                float y = page.getMediaBox().getUpperRightY() - 25;
                stream.newLineAtOffset(25, y); // x from left, y from bottom
                stream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
                stream.setLeading(FONT_SIZE); // for newLine()
                stream.showText("This is a text");
                stream.newLine();
                stream.showText("This is another text");
                stream.endText();
            }

            doc.addPage(page);
            doc.save("output.pdf");
        }
    }

    private static void readPDF() throws IOException {
        try (PDDocument doc = PDDocument.load(new File("output.pdf"))) {
            PDFTextStripper stripper = new PDFTextStripper();
            String text = stripper.getText(doc);
            System.out.println("TEXT: " + text);
        }
    }
}
